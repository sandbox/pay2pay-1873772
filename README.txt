
INTRODUCTION
------------

This module provides Pay2Pay payment gateway for Drupal Commerce.
Pay2Pay allows you to perform payments using various payment services
like PayPal, WebMoney, QIWI, Yandex.Money, RBK Money, LiqPay etc.

REGISTERING
-----------

Before using that module you need to register on
http://www.pay2pay.com/?page=registration and in the admin panel of the shop
you need to fill fields, which are used during module configuration:
a) Secret key - random symbols used to sign messages during redirect
b) Hidden key - random symbols used to sign hidden messages on Result URL;
c) API key - random symbols used in keys of expanding module functionality
   by module developers.

INSTALLATION
------------

1. Copy module directory in default module directory (sites/all/modules
   by default) and enable it on the modules page (Administration/Modules).
2. Go to the settings page of the payment methods
   (Administration/Store/Configuration/Payment methods).
3. Click on edit link and in the Actions area choose Edit next to
   "Enable payment method: Pay2Pay";
4. Configure payment methods filling all required fields:
   - Merchant ID: ID of your shop in Pay2Pay (you can find that in your
     account details);
   - Secret Key;
   - Hidden Key;
   - Test mode;
5. Copy to settings of your shop on Pay2Pay.com site data from following fields:
   - Success URL: URL to redirect user after success payment;
   - Fail URL: URL to redirect user to when payment failed;
   - Result URL: URL to notify about payment status;
6. Save changes.

To check settings you need to perform test payments (money won't be charged).
In the admin panel of the shop in pay2pay.com site, the status of performed
payments will have status "Completed".
To switch to production mode you need to contact with pay2pay manager.
